# Multi-Screen Setup Script

This Bash script automatically sets up the display configuration for multiple connected screens using the `xrandr` and `arandr` commands.

I use this script with my laptop running the i3 window manager. When I dock my laptop and connect it to an external monitor, I simply run the script to automatically configure the display settings. The script is also helpful when I switch between different monitor configurations, such as when I connect to different projectors for presentations. Overall, this script has made managing my multi-screen setup much easier and more convenient.

## Prerequisites

This script requires the following software to be installed:

- Bash shell
- `xrandr` command
- `arandr` command

## Usage

1. Download the script to your computer
2. Open a terminal and navigate to the directory where the script is located
3. Make the script executable by running `chmod +x multi_screen_setup.sh`
4. Run the script by typing `./multi_screen_setup.sh` in the terminal

The script will determine the number of connected screens and run the appropriate commands to set up the display configuration. If there is only one screen connected, the script will run `xrandr --auto` to automatically configure the screen resolution and orientation. If there are multiple screens connected, the script will run `xrandr --auto` to configure the screens, and then launch the `arandr` graphical user interface tool for further configuration.
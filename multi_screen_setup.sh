#!/bin/bash

screen_nb=$(xrandr | grep " connected" | wc -l);

if [ $screen_nb = "1" ];
then
	xrandr --auto
else
	xrandr --auto && arandr
fi